# API de NodeJS utilizando Redis en el cache

Esta API es un ejemplo práctico de la utilización de Redis dentro de una API de NodeJS.

## Endpoints

### /data

Este endpoint obtiene los datos de 1000 compañias generadas por [fakerAPI](https://fakerapi.it/en) y los almacena en Redis de forma local. (127.0.0.1:6379). Teniendo un tiempo de respuesta medio de 5s.

_Es necesario tener levantado Redis para el funcionamiento de la API en local_

Al volver a consultar este endpoint, se obtiene data desde Redis, disminuyendo el tiempo de respuesta a aproximadamente 80ms.

### /delete

Este endpoint verifica si existe en Redis (cache) la variable **data** y en caso de que exista elimina el par clave/valor.
