const express = require("express");
const axios = require("axios");
const responseTime = require("response-time");
const redis = require("redis");

const app = express();

//Para obtener el response-time de la petición
app.use(responseTime());

//config de redis
const client = redis.createClient();

client.on("error", (err) => {
  console.log("Error occured while connecting or accessing redis server");
});

client.connect();

app.get("/data", async (req, res) => {
  try {
    const data = await client.get("data");

    if (data !== null) {
      //Existe en redis (cache)
      return res
        .status(200)
        .send(
          `Se ha encontrado el resultado en Redis: ${JSON.stringify(
            JSON.parse(data)
          )}`
        );
    } else {
      // Realizar consulta si no lo encuentra en Redis
      const response = await axios.get(
        "https://fakerapi.it/api/v1/companies?_quantity=1000"
      );

      // Almacenar la respuesta en Redis
      client.set("data", JSON.stringify(response.data), (error, reply) => {
        if (error) {
          console.error("Ocurrió un error al almacenar en Redis:", error);
        } else {
          console.log("Datos almacenados en Redis correctamente");
        }
      });

      res
        .status(200)
        .send(
          `Datos almacenados en Redis. Recargue la página para ver el resultado`
        );
    }
  } catch (error) {
    console.error("Ocurrió un error al obtener datos:", error);
    res.status(500).send("Error interno del servidor");
  }
});

app.get("/delete", async (req, res) => {
  try {
    const exists = await client.exists("data");
    if (exists === 1) {
      client.del("data");
      res.status(200).send("Se elimina del cache");
    } else {
      res
        .status(200)
        .send(`No existe "data" en el cache. Recarga el endpoint /data.`);
    }
  } catch (error) {
    console.error(error);
  }
});

app.listen(3000);
console.log("Server on port 3000");
